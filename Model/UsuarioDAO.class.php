<?php

include_once 'BD.class.php';

class UsuarioDAO extends BD{
private $bd; //conexão com o banco
private $tabela; //nome da tabela

    public function __construct() {
        $this->bd = new BD();
        $this->tabela = "usuario";
    }

    public function __destruct() {
        unset($this->bd);
    }

//insere um novo registro na tabela usuário      
    public function inserir($usuario) {
		
		$nome=$usuario->getNome();
		$endereco=$usuario->getEndereco();
		$email=$usuario->getemail();
		$sexo=$usuario->getSexo();
		$cpf=$usuario->getCPF();

        $sql = ("INSERT INTO $this->tabela (nome,endereco,email,sexo, cpf ) values ( :nome,:endereco,:email,:sexo,:cpf)");  

     
        $retorno = $this->bd->pdo->prepare($sql);
        $retorno->bindParam(':nome', $nome);
        $retorno->bindParam(':endereco', $endereco);
        $retorno->bindParam(':email', $email);
        $retorno->bindParam(':sexo', $sexo);
        $retorno->bindParam(':cpf', $cpf);

           //print_r($sql); die;

         return $retorno->execute();

              }
          
        /*
        $sql->execute(array(   
         ':nome'  $nome;
         ':endereco'-> $endereco;
         ':email'-> $email;
         ':sexo'-> $sexo; 
        ));
            
            } 
  
    
       
        
     }*/
    
           
   
    public function getAll(){
        $resultado = $this->bd->pdo->query("SELECT * FROM $this->tabela");
        $dados= $resultado->fetchAll(); 

        //print_r($resultado); 
        return $dados;
        
    }

    
    public function excluir($cd_usuario) {

        $sql = "delete from $this->tabela where cd_usuario='$cd_usuario'";
        $retorno= $this->bd->pdo->exec($sql);
        //$retorno = pg_query($sql);
        return $retorno;

    }

      /*  
    //retornar código do ultimo usuario
    
    public function retornaUltimo() {
        $sql = "select max(cd_usuario) FROM $this->tabela";
        $resultado = pg_query($sql);
        $retorno = pg_fetch_array($resultado);
        $ultimo=$retorno['cd_usuario'];

        return $ultimo;
 
    }
   */
    public function listar($cd_usuario){
        try {

           // echo $cd_usuario;
        $sql = "SELECT * FROM $this->tabela WHERE cd_usuario = :cod";


           $res  =$this->bd->pdo->prepare($sql);
           
           $res->bindValue(':cod', $cd_usuario);
           $res->execute();
           
          $linha= $res->fetch();

          //print_r($linha);
          return $linha;
           
           
        } catch ( PDOException  $e) {

           print "Erro: Código:" . $e->getCode() . "Mensagem" . $e->getMessage(); }


    }


    public function editar($usuario){
     //   var_dump($listar);
        $nome=$usuario->getNome();
        $endereco=$usuario->getEndereco();
        $sexo=$usuario->getSexo();
        $cpf=$usuario->getCpf();
        $email=$usuario->getEmail();
        $cd_usuario=$usuario->getcd_usuario();
               
        
        $msq = "UPDATE $this->tabela SET nome=:nome, email=:email, sexo=:sexo, cpf=:cpf, endereco=:endereco where cd_usuario=:cd_usuario";

        $retorno = $this->bd->pdo->prepare($msq);
        $retorno->bindParam(':nome', $nome);
        $retorno->bindParam(':endereco', $endereco);
        $retorno->bindParam(':email', $email);
        $retorno->bindParam(':sexo', $sexo);
        $retorno->bindParam(':cpf', $cpf);
        $retorno->bindParam(':cd_usuario', $cd_usuario);

            //$editar= $this->bd->pdo->exec($msq);
            //$editar = pg_query($msq);
            
            //return $editar;
            return $retorno->execute();
    }

    
}